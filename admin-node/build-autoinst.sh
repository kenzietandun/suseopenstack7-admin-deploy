#!/bin/bash

source ./vars.conf

if [[ "$1" == '--1d' ]]
then
  cp autoinst-1d.template autoinst.xml
elif [[ "$2" == '--2d' ]]
then
  cp autoinst.template autoinst.xml
fi


sed -i "s/SLES12_REG/$SLES12_REG/g" autoinst.xml
sed -i "s/SOC7_REG/$SOC7_REG/g" autoinst.xml
sed -i "s/ROOT_PASS/$ROOT_PASS/g" autoinst.xml
sed -i "s/HTTP_REPO/$HTTP_REPO/g" autoinst.xml
