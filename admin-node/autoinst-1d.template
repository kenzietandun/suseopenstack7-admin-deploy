<?xml version="1.0"?>
<!DOCTYPE profile>
<profile xmlns="http://www.suse.com/1.0/yast2ns" xmlns:config="http://www.suse.com/1.0/configns">
  <bootloader>
    <global>
      <activate>true</activate>
      <append>splash=silent quiet showopts</append>
      <boot_boot>false</boot_boot>
      <boot_extended>false</boot_extended>
      <boot_mbr>false</boot_mbr>
      <boot_root>true</boot_root>
      <generic_mbr>true</generic_mbr>
      <gfxmode>auto</gfxmode>
      <hiddenmenu>false</hiddenmenu>
      <os_prober>false</os_prober>
      <terminal>gfxterm</terminal>
      <timeout config:type="integer">1</timeout>
      <trusted_grub>false</trusted_grub>
    </global>
    <loader_type>grub2</loader_type>
  </bootloader>
  <deploy_image>
    <image_installation config:type="boolean">false</image_installation>
  </deploy_image>
  <general>
    <ask-list config:type="list"/>
    <mode>
      <confirm config:type="boolean">false</confirm>
      <final_halt config:type="boolean">false</final_halt>
      <final_reboot config:type="boolean">true</final_reboot>
      <halt config:type="boolean">false</halt>
      <second_stage config:type="boolean">true</second_stage>
    </mode>
    <proposals config:type="list"/>
    <signature-handling>
      <accept_file_without_checksum config:type="boolean">false</accept_file_without_checksum>
      <accept_non_trusted_gpg_key config:type="boolean">false</accept_non_trusted_gpg_key>
      <accept_unknown_gpg_key config:type="boolean">false</accept_unknown_gpg_key>
      <accept_unsigned_file config:type="boolean">false</accept_unsigned_file>
      <accept_verification_failed config:type="boolean">false</accept_verification_failed>
      <import_gpg_key config:type="boolean">false</import_gpg_key>
    </signature-handling>
    <storage/>
  </general>
  <groups config:type="list"/>
  <host>
    <hosts config:type="list">
      <hosts_entry>
        <host_address>127.0.0.1</host_address>
        <names config:type="list">
          <name>localhost </name>
        </names>
      </hosts_entry>
      <hosts_entry>
        <host_address>192.168.124.10</host_address>
        <names config:type="list">
          <name>admin.openstack.suse admin</name>
        </names>
      </hosts_entry>
    </hosts>
  </host>
  <login_settings/>
  <networking>
    <dns>
      <dhcp_hostname config:type="boolean">false</dhcp_hostname>
      <domain>openstack.suse</domain>
      <hostname>admin</hostname>
      <nameservers config:type="list">
        <nameserver>8.8.8.8</nameserver>
        <nameserver>8.8.4.4</nameserver>
      </nameservers>
      <resolv_conf_policy>auto</resolv_conf_policy>
      <searchlist config:type="list">
        <search>openstack.suse</search>
      </searchlist>
      <write_hostname config:type="boolean">false</write_hostname>
    </dns>
    <interfaces config:type="list">
      <interface>
        <bootproto>static</bootproto>
        <device>eth0</device>
        <ipaddr>192.168.124.10</ipaddr>
        <netmask>255.255.255.0</netmask>
        <startmode>auto</startmode>
      </interface>
      <interface>
        <bootproto>static</bootproto>
        <device>lo</device>
        <firewall>no</firewall>
        <ipaddr>127.0.0.1</ipaddr>
        <netmask>255.0.0.0</netmask>
        <network>127.0.0.0</network>
        <prefixlen>8</prefixlen>
        <startmode>nfsroot</startmode>
        <usercontrol>no</usercontrol>
      </interface>
    </interfaces>
    <ipv6 config:type="boolean">false</ipv6>
    <keep_install_network config:type="boolean">true</keep_install_network>
    <managed config:type="boolean">false</managed>
    <routing>
      <ipv4_forward config:type="boolean">false</ipv4_forward>
      <ipv6_forward config:type="boolean">false</ipv6_forward>
      <routes config:type="list">
        <route>
          <destination>default</destination>
          <device>eth0</device>
          <gateway>192.168.124.200</gateway>
          <netmask>-</netmask>
        </route>
      </routes>
    </routing>
  </networking>
  <partitioning config:type="list">
    <drive>
      <disklabel>msdos</disklabel>
      <enable_snapshots config:type="boolean">true</enable_snapshots>
      <initialize config:type="boolean">true</initialize>
      <partitions config:type="list">
        <partition>
          <create config:type="boolean">true</create>
          <crypt_fs config:type="boolean">false</crypt_fs>
          <filesystem config:type="symbol">ext4</filesystem>
          <format config:type="boolean">true</format>
          <loop_fs config:type="boolean">false</loop_fs>
          <mount>/</mount>
          <mountby config:type="symbol">device</mountby>
          <partition_id config:type="integer">131</partition_id>
          <partition_nr config:type="integer">1</partition_nr>
          <resize config:type="boolean">false</resize>
          <size>15G</size>
        </partition>
        <partition>
          <create config:type="boolean">true</create>
          <crypt_fs config:type="boolean">false</crypt_fs>
          <filesystem config:type="symbol">xfs</filesystem>
          <format config:type="boolean">true</format>
          <loop_fs config:type="boolean">false</loop_fs>
          <mount>/srv</mount>
          <mountby config:type="symbol">device</mountby>
          <partition_id config:type="integer">131</partition_id>
          <partition_nr config:type="integer">1</partition_nr>
          <resize config:type="boolean">false</resize>
          <size>max</size>
        </partition>
      </partitions>
      <pesize/>
      <type config:type="symbol">CT_DISK</type>
      <use>all</use>
    </drive>
  </partitioning>
  <software>
    <image/>
    <install_recommended config:type="boolean">true</install_recommended>
    <instsource/>
    <patterns config:type="list">
      <pattern>base</pattern>
      <pattern>Minimal</pattern>
      <pattern>smt</pattern>
    </patterns>
    <packages  config:type="list">
      <package>apache2</package>
    </packages>
    <!--
    <post-patterns config:type="list">
      <pattern>cloud_admin</pattern>
    </post-patterns>
    -->
  </software>
  <ssh_import>
    <copy_config config:type="boolean">false</copy_config>
    <import config:type="boolean">false</import>
  </ssh_import>
  <timezone>
    <hwclock>UTC</hwclock>
    <timezone>Asia/Jakarta</timezone>
  </timezone>
  <user_defaults>
    <expire/>
    <group>100</group>
    <groups/>
    <home>/home</home>
    <inactive>-1</inactive>
    <no_groups config:type="boolean">true</no_groups>
    <shell>/bin/bash</shell>
    <skel>/etc/skel</skel>
    <umask/>
  </user_defaults>
  <users config:type="list">
    <user>
      <encrypted config:type="boolean">false</encrypted>
      <fullname>root</fullname>
      <gid>0</gid>
      <home>/root</home>
      <password_settings>
        <expire/>
        <flag/>
        <inact/>
        <max/>
        <min/>
        <warn/>
      </password_settings>
      <shell>/bin/bash</shell>
      <uid>0</uid>
      <user_password>ROOT_PASS</user_password>
      <username>root</username>
    </user>
  </users>
  <services-manager>
    <default_target>multi-user</default_target>
    <services>
      <enable config:type="list">
        <service>sshd</service>
        <service>mysql</service>
      </enable>
    </services>
  </services-manager>
  <scripts>
    <init-scripts config:type="list">
      <script>
        <location>HTTP_REPO/init.sh</location>
      </script> 
    </init-scripts>
  </scripts>
  <!--
  <add-on>
    <add_on_products config:type="list">
      <listentry>
        <media_url>HTTP_REPO/soc7/</media_url>
        <alias>suse-cloud-7</alias>
        <product_dir>/</product_dir>
        <priority config:type="integer">20</priority>
        <ask_on_error config:type="boolean">true</ask_on_error>
        <confirm_license config:type="boolean">false</confirm_license>
        <name>suse-cloud-7</name>
      </listentry>
    </add_on_products>
  </add-on>
  -->
    <!--
  <suse_register>
    <do_registration config:type="boolean">true</do_registration>
    <reg_code>SLES12_REG</reg_code>
    <install_updates config:type="boolean">false</install_updates>
    <addons config:type="list">
      <addon>
        <name>suse-openstack-cloud</name>
        <version>7</version>
        <arch>x86_64</arch>
        <reg_code>SOC7_REG</reg_code>
      </addon>
    </addons>
    <slp_discovery config:type="boolean">false</slp_discovery>
  </suse_register>
    -->
</profile>
