#!/bin/bash

source ./vars.conf

if [[ -z "$1" ]]
then
  echo 'Usage: deploy.sh --1d'
  echo '           if the node has 1 disk, or'
  echo '       deploy.sh --2d'
  echo '           if the node has 2 disks'
  exit 1
fi

if [[ "$1" == '--1d' ]]
then
  ./build-autoinst.sh --1d
elif [[ "$1" == '--2d' ]]
then
  ./build-autoinst.sh --2d
fi

./build-init.sh
scp ./autoinst.xml "${SSH_REPO}:/srv/www/repo/"
scp ./init.sh "${SSH_REPO}:/srv/www/repo/"

rm autoinst.xml init.sh
