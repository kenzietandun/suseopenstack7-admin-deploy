#!/bin/bash

source ./vars.conf
./build-autoinst.sh
./build-init.sh
scp ./autoinst-sm.xml "${SSH_REPO}:/srv/www/repo/"
scp ./init-sm.sh "${SSH_REPO}:/srv/www/repo/"

rm autoinst-sm.xml init-sm.sh 
