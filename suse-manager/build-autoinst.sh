#!/bin/bash

source ./vars.conf

cp autoinst.template autoinst-sm.xml

sed -i "s/SLES12_SP1_REG/$SLES12_SP1_REG/g" autoinst-sm.xml
sed -i "s/SM_REG/$SM_REG/g" autoinst-sm.xml
sed -i "s/ROOT_PASS/$ROOT_PASS/g" autoinst-sm.xml
sed -i "s/HTTP_REPO/$HTTP_REPO/g" autoinst-sm.xml
