#!/bin/bash

source ./vars.conf
./build-pxe-default.sh

#for dir in *
#do
#  if [[ -d "$dir" ]]
#  then
#    if [[ -f "${dir}/deploy.sh" ]]
#    then
#      cd "$dir"
#      ./deploy.sh
#      cd ..
#    fi
#  fi
#done

scp ./default "${SSH_REPO}:/srv/tftpboot/pxelinux.cfg/"
scp ./f1.txt "${SSH_REPO}:/srv/tftpboot/f1.txt"
scp ./dhcpd.conf "${SSH_REPO}:/etc/"

rm default f1.txt
