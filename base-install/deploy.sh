#!/bin/bash

source ./vars.conf
./build-autoinst.sh
./build-init.sh
scp ./autoinst-base.xml "${SSH_REPO}:/srv/www/repo/"
scp ./init-base.sh "${SSH_REPO}:/srv/www/repo/"

rm autoinst-base.xml init-base.sh 
