### Requirements

admin-node: 
- sles12 sp2 ISO file
- suse openstack cloud 7 ISO file
- repo.tar file which contains all rpms for OpenStack installation
  * repo.tar can be generated using gen-new-repo script

### Network Topology
```
            +---------------------------+
            |                           |     
Internet ---+ eth0    PXE Server   eth1 +---- Internal Network
            |                           |     192.168.124.0/24
            | DHCP           192.168.124.200
            +---------------------------+


```

PXE Server is setup with 2 interfaces: 
- eth0 connected to internet capable network
- eth1 to a deployment network with IP 192.168.124.200/24

### DHCP/PXE server tree
```
linux-74uc:/srv/tftpboot # tree
.
├── f1.txt
├── pxelinux.0
├── pxelinux.cfg
│   └── default
└── sles12sp2
    ├── initrd
    └── linux

2 directories, 5 files


linux-74uc:/srv/www/repo # tree -d -L 2
.
├── sles12sp2
│   ├── EFI
│   ├── boot
│   ├── docu
│   ├── media.1
│   └── suse
├── soc7
│   ├── docu
│   ├── license
│   ├── media.1
│   └── suse
└── templates

12 directories
```

### Usage

To use the script, edit vars.conf.template, then rename it to vars.conf

Then run `./deploy.sh`
