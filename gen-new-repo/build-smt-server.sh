#!/bin/bash

source ./vars.conf
cp ./smt.template ./smt-server-vars.yml
sed -i "s/{{ HTTP_REPO }}/$HTTP_REPO/g"  smt-server-vars.yml
sed -i "s/{{ NUUser }}/$NUUSER/g"  smt-server-vars.yml
sed -i "s/{{ NUPass }}/$NUPASS/g"  smt-server-vars.yml
sed -i "s/{{ DBpass }}/$DBPASS/g"  smt-server-vars.yml
sed -i "s/{{ LOCALnccEmail }}/$LOCALNCCEMAIL/g"  smt-server-vars.yml

mv smt-server-vars.yml smt.conf
