#!/bin/bash

source ./vars.conf
./build-autoinst.sh
./build-smt-server.sh
./build-init.sh
scp ./autoinst-genrepo.xml "${SSH_REPO}:/srv/www/repo/"
scp ./init-genrepo.sh "${SSH_REPO}:/srv/www/repo/"
scp ./smt.conf "${SSH_REPO}:/srv/www/repo/templates/"

rm autoinst-genrepo.xml init-genrepo.sh smt.conf
