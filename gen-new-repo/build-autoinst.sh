#!/bin/bash

source ./vars.conf

cp autoinst.template autoinst-genrepo.xml

sed -i "s/SLES12_REG/$SLES12_REG/g" autoinst-genrepo.xml
sed -i "s/SOC7_REG/$SOC7_REG/g" autoinst-genrepo.xml
sed -i "s/SES4_REG/$SES4_REG/g" autoinst-genrepo.xml
sed -i "s/SLE_HA_REG/$SLE_HA_REG/g" autoinst-genrepo.xml
sed -i "s/ROOT_PASS/$ROOT_PASS/g" autoinst-genrepo.xml
sed -i "s/HTTP_REPO/$HTTP_REPO/g" autoinst-genrepo.xml
